'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('companies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(10)
      },
      company_name: {
        type: Sequelize.STRING(50)
      },
      telephone_number: {
        type: Sequelize.STRING(16),
        defaultValue: null
      },
      is_active: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      address: {
        type: Sequelize.STRING(50)
      }
    }, { timestamps: false });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('companies');
  }
};
'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('employees', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER(10)
      },
      name: {
        type: Sequelize.STRING(50)
      },
      email: {
        type: Sequelize.STRING(255)
      },
      phone_number: {
        type: Sequelize.STRING(16),
        defaultValue: null
      },
      jobtitle: {
        type: Sequelize.ENUM('manager','director','staff'),
        defaultValue: 'staff'
      },
      company_id: {
        type: Sequelize.INTEGER(10),
        references: {
          model: 'companies',
          key: 'id'
        }
      }
    }, { timestamps: false });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('employees');
  }
};
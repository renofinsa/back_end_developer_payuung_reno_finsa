'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    return Promise.all([
      queryInterface.bulkInsert('companies', [
        {
          company_name: 'PT Fatiha Sakti',
          telephone_number: '02183793420',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
        {
          company_name: 'PT Pertamina Persero',
          telephone_number: '02183193420',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
        {
          company_name: 'PT Bank DKI',
          telephone_number: '0218373420',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
        {
          company_name: 'PT Maju Karya',
          telephone_number: '0218379340',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
        {
          company_name: 'PT Mahkota Putra',
          telephone_number: '0218379312',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
        {
          company_name: 'PT Karya Anak Bangsa',
          telephone_number: '02183793421',
          address: 'Lorem ipsum dolor sit amet consectetur.',
          is_active: true
        },
      ]),
      queryInterface.bulkInsert('employees', [
        {
          name: 'John Doe',
          email: 'johndoe@domain.com',
          phone_number: '08111899611',
          jobtitle: 'director',
          company_id: 1,
        },
        {
          name: 'Jean Doe',
          email: 'jeandoe@domain.com',
          phone_number: '08111899612',
          jobtitle: 'manager',
          company_id: 1,
        },
        {
          name: 'Thomas Doe',
          email: 'thomasdoe@domain.com',
          phone_number: '08111899613',
          jobtitle: 'director',
          company_id: 2,
        },
        {
          name: 'Lee Doe',
          email: 'leedoe@domain.com',
          phone_number: '08111899614',
          jobtitle: 'staff',
          company_id: 2,
        },
        {
          name: 'Naka Doe',
          email: 'leedoe@domain.com',
          phone_number: '08111899615',
          jobtitle: 'staff',
          company_id: 1,
        },
      ])
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};

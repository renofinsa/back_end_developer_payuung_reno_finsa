// SEQUELIZE
const Database = require('../Database');
const { Company } = Database;
const sequelize = Database.Sequelize;
const { Op } = sequelize;

exports.get = async () => {
  try {
    var data = await Company.findAll({
      where: {is_active: true},
      order: [['company_name', 'ASC']]
    });
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.getByCompanyId = async (req) => {
  try {
    var data = await Company.findAll({
      where: {id: req.params.id},
      include: [
        {
          association: 'employees',
          attributes: ['name', 'email', 'jobtitle', 'phone_number']
        },
      ]
    });
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.store = async (req) => {
  const {
    company_name,
    telephone_number,
    address
  } = req.body;

  try {
    const data = await Company.create();
    data.company_name = company_name;
    data.telephone_number = telephone_number;
    data.address = address;
    data.save();

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.setActive = async (req) => {
  try {
    var data = await Company.findOne({where: {id: req.params.id}})

    await Company.update({ 
      is_active: data.is_active ? false : true
    }, { where: { id: req.params.id }});

    return true;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
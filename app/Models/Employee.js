// SEQUELIZE
const Database = require('../Database');
const { Employee } = Database;
const sequelize = Database.Sequelize;
const { Op } = sequelize;

exports.get = async () => {
  try {
    var data = await Employee.findAll({
      order: [['name', 'ASC']],
      include: [
        {
          association: 'company',
          attributes: ['company_name']
        },
      ]
    });
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.getById = async (req) => {
  try {
    var data = await Employee.findOne({
      where: {id: req.params.id},
      include: [
        {
          association: 'company',
          attributes: ['company_name']
        },
      ]
    });
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.destroy = async (req) => {
  try {
    var data = await Employee.destroy({
      where: {id: req.params.id}
    });
    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.store = async (req) => {
  const {
    name,
    email,
    phone_number,
    jobtitle
  } = req.body;

  try {
    const data = await Employee.create();
    data.name = name;
    data.email = email;
    data.phone_number = phone_number;
    data.jobtitle = jobtitle;
    data.company_id = req.params.company_id
    data.save();

    return data;
  } catch (error) {
    console.log(error);
    throw error;
  }
};

exports.update = async (req) => {
  const {
    name,
    email,
    phone_number,
    jobtitle
  } = req.body;

  try {
    await Employee.update({ 
      name: name,
      email: email,
      phone_number: phone_number,
      jobtitle: jobtitle
    }, { where: { id: req.params.employee_id }});

    return true;
  } catch (error) {
    console.log(error);
    throw error;
  }
};
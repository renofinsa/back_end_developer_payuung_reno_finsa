module.exports = () => {
  const controller = require('./../Controllers/EmployeesController');
  const app = require('express').Router();

  app.get('/', controller.get);
  app.get('/:id', controller.getById);
  app.delete('/:id', controller.destroyById);

  return app;
};

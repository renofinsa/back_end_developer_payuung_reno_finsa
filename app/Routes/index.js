const app = require('express').Router()
const routes = (name) => require(`./${name}`);

app.get('/', (req, res) => {
  return res.success({message: 'Lets Code'})
})

app.use('/', routes('Playground')());
app.use('/', routes('Countries')());
app.use('/companies', routes('Companies')());
app.use('/employees', routes('Employees')());

module.exports = app

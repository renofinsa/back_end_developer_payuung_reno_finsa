module.exports = () => {
  const controller = require('./../Controllers/CountriesController');
  const app = require('express').Router();

  // Exam 5
  app.get('/countries', controller.countries);

  return app;
};

module.exports = () => {
  const controller = require('./../Controllers/PlaygroundController');
  const app = require('express').Router();

  // Exam 1
  app.post('/reverse', controller.reverse);

  // Exam 2
  app.post('/fibonacci', controller.fibonacci);
  
  // Exam 3
  app.post('/combination', controller.combination);

  return app;
};

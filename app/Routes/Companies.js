module.exports = () => {
  const controller = require('./../Controllers/CompaniesController');
  const EmployeesController = require('./../Controllers/EmployeesController');
  const app = require('express').Router();

  // Exam 5
  app.get('/', controller.get);
  app.post('/', controller.store);
  app.get('/:id/employees', controller.getByCompanyId);
  app.put('/:id/set_active', controller.setActive);
  app.post('/:company_id/employees', EmployeesController.store);
  app.put('/:company_id/employees/:employee_id', EmployeesController.update);

  return app;
};

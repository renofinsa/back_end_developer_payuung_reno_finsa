const Joi = require('@hapi/joi');
const ResponseJoi = require('../Helpers/ResponseJoi');

exports.create = async (req) => {
  const { company_name, telephone_number, address } = req.body;

  const schemaRoles = Joi.object({
    company_name: Joi.string().required().max(50).min(3).label("Company name"),
    telephone_number: Joi.string().required().max(16).min(8).label("Telephone number"),
    address: Joi.string().max(50).min(10).label("Address")
  });

  const { error } = schemaRoles.validate(
    { company_name, telephone_number, address },
    { abortEarly: false },
  );

  if (error) throw ResponseJoi.message(error.details);
  return true;
};
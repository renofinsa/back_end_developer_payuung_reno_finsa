const Joi = require('@hapi/joi');
const ResponseJoi = require('../Helpers/ResponseJoi');

exports.create = async (req) => {
  const { name, email, phone_number, jobtitle } = req.body;

  const schemaRoles = Joi.object({
    name: Joi.string().required().max(50).min(2).label("Name"),
    email: Joi.string().required().max(255).min(5).label("Email"),
    phone_number: Joi.string().required().max(16).min(8).label("Telephone number"),
    jobtitle: Joi.string().required().label("Job title")
  });

  const { error } = schemaRoles.validate(
    { name, email, phone_number, jobtitle },
    { abortEarly: false },
  );

  if (error) throw ResponseJoi.message(error.details);
  return true;
};
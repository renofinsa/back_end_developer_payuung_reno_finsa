exports.message = (error) => {
  var data = [];
  error.forEach(item => {
    var payload = {
      key: item.context.key,
      message: item.message
    }
    data.push(payload)
  });
  return data
};
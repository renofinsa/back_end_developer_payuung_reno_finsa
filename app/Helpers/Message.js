exports.error = (error) => {
  const msg = error.toString();
  const reg = /(?:\[object Object\])+/g;

  return reg.exec(msg) ? error : error.toString();
};

exports.get = (param) => {
  const data = {
    invalidParamLogin: 'Invalid Username or Password !',
  };

  return data[param];
};

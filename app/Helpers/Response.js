const errorMsg = (error) => {
  const msg = error.toString();
  const reg = /(?:\[object Object\])+/g;

  return reg.exec(msg) ? error : error.toString();
};
module.exports = (req, res, next) => {
  /**
   *
   * @code {200,202,400,dll} status code http
   * @message {message, warning, dll}
   * @data {data output}
   */
  res.success = ({ code = 200, success = true, error = null, message, data = '' }) => {
    res.status(code).send({
      success,
      error,
      message,
      data,
    });
  };
  /**
   * @error {obj.data, obj.code} ex: error.msg="pesan error", error.code=400
   */
  res.error = ({ code = 200, error, message = 'error message' }) => {
    res.status(code).json({
      success: false,
      error,
      message,
      data: [],
    });
  };

  next();
};
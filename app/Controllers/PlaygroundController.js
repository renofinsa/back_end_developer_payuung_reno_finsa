const { reverse, fibonacci, combination } = require('./../Helpers/Main')

// Exam 1
exports.reverse = async (req, res) => {
  var data = await reverse(req.body.character);

  // response
  return res.success({ data, message: 'Reverse' });
};

// Exam 2
exports.fibonacci = async (req, res) => {
  var data = await fibonacci(req.body.n);
  
  // response
  return res.success({ data, message: 'Fibonacci' });
};

// Exam 3
exports.combination = async (req, res) => {
  const { n, r } = req.body;
  var data = await combination(n, r);

  // response
  return res.success({ data, message: 'Combination' });
};
const axios = require('axios')

// Exam 5
exports.countries = async (req, res) => {
  // connected with axios
  const getData = await axios({
    method: 'get',
    url: 'https://restcountries.eu/rest/v2/all'
  });

  // prepare payload result
  var data = [];
  getData.data.map(item => {
    var payload = {
      name: item.name,
      region: item.region,
      timezones: item.timezones
    }
    data.push(payload);
  });

  // response
  return res.success({ data, message: 'Countries' });
};
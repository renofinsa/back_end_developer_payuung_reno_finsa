const Message = require('./../Helpers/Message');
const Employee = require('../Models/Employee');
const Validate = require('./../Validations/EmployeesValidation');

exports.get = async (req, res) => {
  try {
    const data = await Employee.get();

    if (data.length == 0) {
      return res.success({ message: 'Data Not Available', data: [] });
    }

    // response
    return res.success({ message: 'Employess', data });
  } catch (error) {
    console.log(error, '<=== ERROR ===>');
    return res.error({ error });
  }
};

exports.getById = async (req, res) => {
  try {
    const data = await Employee.getById(req);

    if (!data) {
      return res.error({ code: 404, message: 'Not found!', error: 'Error 404' });
    }

    // response
    return res.success({ message: 'Detail Employee', data });
  } catch (error) {
    console.log(error, '<=== ERROR ===>');
    return res.error({ error });
  }
};

exports.destroyById = async (req, res) => {
  try {
    const data = await Employee.destroy(req);

    if (!data) {
      return res.error({ code: 404, message: 'Not found!', error: 'Error 404' });
    }

    return res.success({ message: 'Data has been deleted', data });
  } catch (error) {
    console.log(error, '<=== ERROR ===>');
    return res.error({ error });
  }
};

exports.store = async (req, res) => {
  try {
    // validate request
    await Validate.create(req);

    // query model
    const data = await Employee.store(req);

    // response
    return res.success({ data, message: 'Data has been created' });
  } catch (error) {
    return res.error({ code: 503, error: Message.error(error) });
  }
};

exports.update = async (req, res) => {
  try {
    // validate request
    await Validate.create(req);

    // query model
    const data = await Employee.update(req);

    // response
    return res.success({ data, message: 'Data has been updated' });
  } catch (error) {
    return res.error({ code: 503, error: Message.error(error) });
  }
};

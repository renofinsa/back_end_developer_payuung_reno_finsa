const Validate = require('./../Validations/CompaniesValidation');
const Message = require('./../Helpers/Message');
const Company = require('../Models/Company');

exports.get = async (req, res) => {
  try {
    const data = await Company.get();

    if (data.length == 0) {
      return res.success({ message: 'Data Not Available', data: [] });
    }

    // response
    return res.success({ message: 'Companies', data });
  } catch (error) {
    console.log(error, '<=== ERROR ===>');
    return res.error({ error });
  }
};

exports.store = async (req, res) => {
  try {
    // validation request
    await Validate.create(req);

    // query model
    const data = await Company.store(req);

    // response
    return res.success({ data, message: 'Data has been created' });
  } catch (error) {
    return res.error({ code: 503, error: Message.error(error) });
  }
};

exports.getByCompanyId = async (req, res) => {
  try {
    const data = await Company.getByCompanyId(req);

    if (!data) {
      return res.error({ code: 404, message: 'Not found!', error: 'Error 404' });
    }

    // response
    return res.success({ message: 'Detail Company', data });
  } catch (error) {
    console.log(error, '<=== ERROR ===>');
    return res.error({ error });
  }
};

exports.setActive = async (req, res) => {
  try {
    // query model
    const data = await Company.setActive(req);

    // response
    return res.success({ data, message: 'Status has been changed' });
  } catch (error) {
    return res.error({ code: 503, error: Message.error(error) });
  }
};